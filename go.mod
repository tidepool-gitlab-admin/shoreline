module github.com/tidepool-org/shoreline

go 1.12

require (
	github.com/RangelReale/osin v1.0.1
	github.com/SpeakData/minimarketo v0.0.0-20170821092521-29339e452f44
	github.com/codegangsta/cli v1.20.0
	github.com/dgrijalva/jwt-go v1.0.2
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/google/uuid v1.1.1 // indirect
	github.com/gorilla/mux v1.7.3
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/pborman/uuid v0.0.0-20180906182336-adf5a7427709 // indirect
	github.com/prometheus/client_golang v1.4.1
	github.com/tidepool-org/go-common v0.5.0
)
